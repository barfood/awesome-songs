This simfile was created using ITG2 metrics. It contains all difficulties on single and double pad. The song is
about 1:49 long and the banner is in 418x164 (ITG) dimension. Steps made by me with the ITG step artist label as
my real name, Joe DeGarmo. Don't worry about the weird 5-arrow handplants on Expert Doubles; it can be done using
your feet, hands, and knees. The hand-roll on Expert Singles is a unique step pattern not found in any ITG arcade
song. Graphics created by me using Xara 3D version 6 for the text. Special thanks to SaxxonPike (Sanxion7) for
producing the music. (Brilliant song, man!) The difficulties are 1/4/6/8/10 (singles) and 3/5/9/11 (doubles).
Please enjoy this simfile and have fun!