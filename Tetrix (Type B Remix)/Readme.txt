Please Read:


This work is licensed under the Creative Commons Sampling Plus 1.0 License. To view a copy of this license, visit http://creativecommons.org/licenses/sampling+/1.0/ or send a letter to Creative Commons, 543 Howard Street, 5th Floor, San Francisco, California, 94105, USA.

In accordance with the above license, Hopscotch Mix (as a whole) gives Stepmania.com the right to use this song/stepfile for commercial use in their Stepmania complete CD.  Any other commercial use of this song/stepfile other than by that of the creators (AKA: Hopscotch Mix), is prohibited.  Thank You.

Song: Tetrix
Artist: Hypernov8 (Hopscotch Mix Contributor)